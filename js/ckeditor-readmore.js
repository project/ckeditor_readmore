/**
 * @file
 * CKEditor Read more functionality.
 */

(function ($, Drupal, once) {

  'use strict';

  Drupal.behaviors.ckeditorReadmore = {
    attach: function (context, settings) {
      once('ckeditor-readmore', '.ckeditor-readmore', context).forEach(function (element) {
        // Get the toggler tag and texts.
        var togglerTag = element.dataset.readmoreType === 'button' ? 'button' : 'a';
        var moreText = element.dataset.readmoreMoreText;
        var lessText = element.dataset.readmoreLessText;
        var classes = element.dataset.readmoreClasses;

        // Add the wrapper if missing (CKEditor 4).
        var wrapper = element.parentNode;

        if (!wrapper.classList.contains('ckeditor-readmore-wrapper')) {
          wrapper = document.createElement('div');
          wrapper.classList.add('ckeditor-readmore-wrapper');
          element.parentNode.insertBefore(wrapper, element);
          wrapper.appendChild(element);
        }

        // Add the toggler.
        var toggler = document.createElement(togglerTag);
        toggler.classList.add('ckeditor-readmore-toggler');

        if (classes) {
          classes.split(" ").forEach(function (item) {
            toggler.classList.add(item);
          });
        }

        toggler.innerText = moreText;

        if (togglerTag === 'a') {
          toggler.setAttribute('href', '#');
        }

        wrapper.appendChild(toggler);

        // Bind the event.
        toggler.onclick = function (event) {
          event.preventDefault();

          toggler.blur();
          toggler.classList.toggle('open');
          wrapper.classList.toggle('open');
          $(element).slideToggle();

          if (toggler.innerText.toLowerCase() === moreText.toLowerCase()) {
            toggler.innerText = lessText;
          }
          else {
            toggler.innerText = moreText;
          }
        };
      });
    }
  };

})(jQuery, Drupal, once);
