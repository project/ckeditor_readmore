/**
 * @file defines InsertSimpleBoxCommand, which is executed when the simpleBox
 * toolbar button is pressed.
 */
// cSpell:ignore simpleboxediting

import { Command } from 'ckeditor5/src/core';

export default class InsertReadMoreCommand extends Command {
  execute() {
    const { model } = this.editor;

    model.change((writer) => {
      // Insert <readMore>*</readMore> at the current selection position
      // in a way that will result in creating a valid model structure.
      model.insertContent(createReadMore(writer));
    });
  }

  refresh() {
    const { model } = this.editor;
    const { selection } = model.document;

    // Determine if the cursor (selection) is in a position where adding a
    // readMore is permitted. This is based on the schema of the model(s)
    // currently containing the cursor.
    const allowedIn = model.schema.findAllowedParent(
      selection.getFirstPosition(),
      'readMore',
    );

    // If the cursor is not in a location where a readMore can be added, return
    // null so the addition doesn't happen.
    this.isEnabled = allowedIn !== null;
  }
}

function createReadMore(writer) {
  // Create instances of the elements registered with the editor in
  // readmoreediting.js.
  const readMore = writer.createElement('readMore');
  const readMoreContent = writer.createElement('readMoreContent');

  // Append the content element to the readMore, which matches
  // the parent/child relationship as defined in their schemas.
  writer.append(readMoreContent, readMore);

  // The readMoreContent text content will automatically be wrapped in a
  // `<p>`.
  writer.appendElement('paragraph', readMoreContent);

  // Return the element to be added to the editor.
  return readMore;
}
