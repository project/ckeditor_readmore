import { Plugin } from 'ckeditor5/src/core';
import { toWidget, toWidgetEditable } from 'ckeditor5/src/widget';
import { Widget } from 'ckeditor5/src/widget';
import InsertReadMoreCommand from './insertreadmorecommand';

/**
 * CKEditor 5 plugins do not work directly with the DOM. They are defined as
 * plugin-specific data models that are then converted to markup that
 * is inserted in the DOM.
 *
 * This file has the logic for defining the readMore model, and for how it is
 * converted to standard DOM markup.
 */
export default class ReadMoreEditing extends Plugin {
  static get requires() {
    return [Widget];
  }

  init() {
    this._defineSchema();
    this._defineConverters();
    this.editor.commands.add(
      'insertReadMore',
      new InsertReadMoreCommand(this.editor),
    );
  }

  /*
   * This registers the structure that will be seen by CKEditor 5 as
   * <readMore>
   *    <readMoreContent></readMoreContent>
   * </readMore>
   *
   * The logic in _defineConverters() will determine how this is converted to
   * markup.
   */
  _defineSchema() {
    // Schemas are registered via the central `editor` object.
    const schema = this.editor.model.schema;

    schema.register('readMore', {
      // Behaves like a self-contained object (e.g. an image).
      isObject: true,
      // Allow in places where other blocks are allowed (e.g. directly in the root).
      allowWhere: '$block',
    });

    schema.register('readMoreContent', {
      isObject: true,
      // This is only to be used within readMore.
      allowIn: 'readMore',
      // Allow content that is allowed in the root of the editor.
      allowContentOf: '$root',
      allowAttributes: [
        'data-readmore-type',
        'data-readmore-classes',
      ],
    });

    schema.addChildCheck((context, childDefinition) => {
      // Disallow readMore inside readMoreContent.
      if (
        context.endsWith('readMoreContent') &&
        childDefinition.name === 'readMore'
      ) {
        return false;
      }
    });
  }

  /**
   * Converters determine how CKEditor 5 models are converted into markup and
   * vice-versa.
   */
  _defineConverters() {
    // Converters are registered via the central editor object.
    const { conversion } = this.editor;

    // Upcast Converters: determine how existing HTML is interpreted by the
    // editor. These trigger when an editor instance loads.
    //
    // If <div class="ckeditor-readmore-wrapper"> is present in the existing markup
    // processed by CKEditor, then CKEditor recognizes and loads it as a
    // <readMore> model.
    conversion.for('upcast').elementToElement({
      model: 'readMore',
      view: {
        name: 'div',
        classes: 'ckeditor-readmore-wrapper',
      },
    });

    // If <div class="ckeditor-readmore"> is present in the existing markup
    // processed by CKEditor, then CKEditor recognizes and loads it as a
    // <readMoreContent> model, provided it is a child element of
    // <readMore>, as required by the schema.
    conversion.for('upcast').elementToElement({
      model: 'readMoreContent',
      view: {
        name: 'div',
        classes: 'ckeditor-readmore',
      },
    });

    // Data Downcast Converters: converts stored model data into HTML.
    // These trigger when content is saved.
    //
    // Instances of <readMore> are saved as
    // <div class="ckeditor-readmore-wrapper">{{inner content}}</div>.
    conversion.for('dataDowncast').elementToElement({
      model: 'readMore',
      view: {
        name: 'div',
        classes: 'ckeditor-readmore-wrapper',
      },
    });

    // Instances of <readMoreContent> are saved as
    // <div class="ckeditor-readmore">{{inner content}}</div>.
    conversion.for('dataDowncast').elementToElement({
      model: 'readMoreContent',
      view: (modelElement, { writer: viewWriter }) => {
        const config = this.editor.config.get('readMore');
        return viewWriter.createEditableElement('div', {
          class: 'ckeditor-readmore',
          'data-readmore-type': config.type,
          'data-readmore-classes': config.classes,
        });
      },
    });

    // Editing Downcast Converters. These render the content to the user for
    // editing, i.e. this determines what gets seen in the editor. These trigger
    // after the Data Upcast Converters, and are re-triggered any time there
    // are changes to any of the models' properties.
    //
    // Convert the <readMore> model into a container widget in the editor UI.
    conversion.for('editingDowncast').elementToElement({
      model: 'readMore',
      view: (modelElement, { writer: viewWriter }) => {
        const wrapper = viewWriter.createContainerElement('div', {
          class: 'ckeditor-readmore-wrapper',
        });

        return toWidget(wrapper, viewWriter, { label: 'Read more widget' });
      },
    });

    // Convert the <readMoreContent> model into an editable <div> widget.
    conversion.for('editingDowncast').elementToElement({
      model: 'readMoreContent',
      view: (modelElement, { writer: viewWriter }) => {
        const config = this.editor.config.get('readMore');
        const content = viewWriter.createEditableElement('div', {
          class: 'ckeditor-readmore',
          'data-readmore-type': config.type,
          'data-readmore-classes': config.classes,
        });
        return toWidgetEditable(content, viewWriter);
      },
    });
  }
}
