/**
 * @file registers the readMore toolbar button and binds functionality to it.
 */

import { Plugin } from 'ckeditor5/src/core';
import { ButtonView } from 'ckeditor5/src/ui';
import icon from '../../../../icons/readmore.svg';

export default class ReadMoreUI extends Plugin {
  init() {
    const editor = this.editor;

    // This will register the readMore toolbar button.
    editor.ui.componentFactory.add('readMore', (locale) => {
      const command = editor.commands.get('insertReadMore');
      const buttonView = new ButtonView(locale);

      // Create the toolbar button.
      buttonView.set({
        label: editor.t('Read more'),
        icon,
        tooltip: true,
      });

      // Bind the state of the button to the command.
      buttonView.bind('isOn', 'isEnabled').to(command, 'value', 'isEnabled');

      // Execute the command when the button is clicked (executed).
      this.listenTo(buttonView, 'execute', () =>
        editor.execute('insertReadMore'),
      );

      return buttonView;
    });
  }
}
