# CKEditor Read More

This module adds a new element to CKEditor which allows users to hide selected
content and toggle it with "Read more" element click.

In the v3 we dropped the CKEditor4 support.

## Table of contents

- Installation
- Usage
- Configuration

## Installation

1.  Enable the module
2.  Drag and drop the Read More button to the CKEditor toolbar (admin/config/content/formats).
3.  Enable the filter "Filter readmore".
4.  If you have enabled "Limit allowed HTML tags" filter in Text formats and editors settings, check if you have <div data-readmore-less-text data-readmore-more-text data-readmore-type class="ckeditor-readmore"> present in the "Allowed HTML tags" section. Add it if it was not automatically added.
5.  Save the text format configuration and clear the cache.

## Usage

Select the text you want to hide and press the "Read more" button in CKEditor
toolbar.

## Configuration

In editor setting you can choose between "Button" and "Text" type of toggling
element under text. You can also modify "Read more" and "Show less" translatable
messages.

## Development Notes

To install the dependencies run `npm install`.

After installing dependencies, plugins can be built with `npm run build` or
`npm run watch`. They will be built to `js/build/{pluginNameDirectory}.js`.
