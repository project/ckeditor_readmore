<?php

namespace Drupal\ckeditor_readmore\Plugin\CKEditor5Plugin;

use Drupal\ckeditor5\Plugin\CKEditor5PluginConfigurableInterface;
use Drupal\ckeditor5\Plugin\CKEditor5PluginConfigurableTrait;
use Drupal\ckeditor5\Plugin\CKEditor5PluginDefault;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\EditorInterface;

/**
 * CKEditor ReadMore plugin configuration.
 *
 * @internal
 *   Plugin classes are internal.
 */
class ReadMore extends CKEditor5PluginDefault implements CKEditor5PluginConfigurableInterface {

  use CKEditor5PluginConfigurableTrait;

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Type of read more element'),
      '#description' => $this->t('Choose between plain text and button'),
      '#required' => TRUE,
      '#options' => [
        'text' => $this->t('Plain text'),
        'button' => $this->t('Button')
      ],
      '#default_value' => $this->configuration['type'],
    ];

    $form['classes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Classes for the element'),
      '#description' => $this->t('Add classes to read more/less element. Separate classes by a whitespace.'),
      '#default_value' => $this->configuration['classes'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['type'] = $form_state->getValue('type');
    $this->configuration['classes'] = $form_state->getValue('classes');
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'type' => 'text',
      'classes' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getDynamicPluginConfig(array $static_plugin_config, EditorInterface $editor): array {
    return [
      'readMore' => [
        'type' => $this->configuration['type'],
        'classes' => $this->configuration['classes'],
      ],
    ];
  }

}
