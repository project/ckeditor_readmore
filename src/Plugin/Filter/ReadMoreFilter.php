<?php

declare(strict_types=1);

namespace Drupal\ckeditor_readmore\Plugin\Filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\Component\Utility\Html;

/**
 * Provides a filter to attach the custom library to readmore.
 * Example -> https://www.lullabot.com/articles/creating-a-custom-filter-in-drupal-8
 *
 * @Filter(
 *   id = "filter_readmore",
 *   title = @Translation("Filter readmore"),
 *   description = @Translation("Attach ckeditor_readmore related library to elements."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE
 * )
 */
final class ReadMoreFilter extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['more_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text in read more element'),
      '#description' => $this->t('This text shows up in read more element'),
      '#required' => TRUE,
      '#size' => 60,
      '#maxlength' => 128,
      '#default_value' => $this->settings['more_text'] ?? 'Read more',
    ];

    $form['less_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text in show less element'),
      '#description' => $this->t('This text shows up in show less element'),
      '#required' => TRUE,
      '#size' => 60,
      '#maxlength' => 128,
      '#default_value' => $this->settings['less_text'] ?? 'Show less',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $result = new FilterProcessResult($text);
    if (stristr($text, 'data-readmore-type') !== FALSE
        && !array_search('ckeditor_readmore/ckeditor_readmore', $result->getAttachments()['library'] ?? [])) {

      $read_more_text = t('Read more');
      if (!empty($this->settings['more_text'])) {
        $read_more_text = t($this->settings['more_text']);
      }
      $read_less_text = t('Show less');
      if (!empty($this->settings['less_text'])) {
        $read_less_text = t($this->settings['less_text']);
      }

      $dom = Html::load($text);
      foreach ($dom->getElementsByTagName('div') as $item) {
        if ($item->hasAttribute('data-readmore-type')) {
          $item->setAttribute('data-readmore-more-text', $read_more_text->render());
          $item->setAttribute('data-readmore-less-text', $read_less_text->render());
        }
      }

      $result
        ->setProcessedText(Html::serialize($dom))
        ->addAttachments([
          'library' => [
            'ckeditor_readmore/ckeditor_readmore',
          ],
        ]);

    }
    return $result;
  }
}
